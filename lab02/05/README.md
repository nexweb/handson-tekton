
kubectl create secret generic quay-credentials \
--from-literal=quay-username='test' \
--from-literal=quay-password='test@'


## 기존의 자격 증명을 기반으로 시크릿 생성하기
```bash
kubectl create secret generic regcred \
--from-file=.dockerconfigjson=$HOME/.docker/config.json> \
--type=kubernetes.io/dockerconfigjson
```

## 시크릿을 사용하는 파드 생성하기
```YAML
apiVersion: v1
kind: Pod
metadata:
  name: private-reg
spec:
  containers:
  - name: private-reg-container
    image: <your-private-image>
  imagePullSecrets:
  - name: regcred
```
