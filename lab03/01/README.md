
## Install Tasks from Catalog

* git-clone to clone the source code
```bash
tkn hub info task git-clone
tkn hub install task git-clone --version 0.8
```

* npm to run tests
```bash
tkn hub info task npm
tkn hub install task npm --version 0.1
```


* codecov for code coverage
```bash
tkn hub info task codecov
tkn hub install task codecov --version 0.1
```

* buildah to build and push the image to container registry
```bash
tkn hub info task buildah
tkn hub install task buildah --version 0.5
```

* kubernetes-actions to check whether there is an existing deployment or not.
```
tkn hub info task kubernetes-actions
tkn hub install  task kubernetes-actions --version 0.2
```

