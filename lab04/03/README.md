tkn pipeline start githhub-to-openshift-build-and-deploy 
    -w name=common-workspace,volumeClaimTemplateFile=https://raw.githubusercontent.com/openshift/pipelines-tutorial/pipelines-1.7/01_pipeline/03_persistent_volume_claim.yaml 
    -p deployment-name=pipelines-vote-api 
    -p git-url=https://github.com/openshift/pipelines-vote-api.git 
    -p IMAGE=image-registry.openshift-image-registry.svc:5000/pipelines-tutorial/pipelines-vote-api 
    --use-param-defaults
