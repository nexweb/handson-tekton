# handson-tekton

handson-tekton


## Getting started

## Create a Hello World task

```yaml
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: hello
spec:
  steps:
    - name: say-hello
      image: registry.access.redhat.com/ubi8/ubi
      command:
        - /bin/bash
      args: ['-c', 'echo Hello World']
```

Apply this Task to your cluster
```bash
kubectl apply -f ./lab01/01/demo/hello.yaml
tkn task start --showlog hello
```
